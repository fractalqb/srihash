package main

// https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity

import (
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"flag"
	"fmt"
	"hash"
	"io"
	"log"
	"os"
)

var (
	hashName string = "sha384"
	hashFunc hash.Hash
	htmlElem string

	b64enc = base64.StdEncoding
	hashes = map[string]hash.Hash{
		"sha256": sha256.New(),
		"sha384": sha512.New384(),
		"sha512": sha512.New(),
	}
)

func hasher() hash.Hash {
	if hashFunc == nil {
		if hashFunc = hashes[hashName]; hashFunc == nil {
			log.Fatalf("Unknown digest function '%s'", hashName)
		}
	} else {
		hashFunc.Reset()
	}
	return hashFunc
}

func digest(rd io.Reader) []byte {
	hashFunc = hasher()
	if _, err := io.Copy(hashFunc, rd); err != nil {
		log.Fatal(err)
	}
	return hashFunc.Sum(nil)
}

func digestFile(name string) []byte {
	rd, err := os.Open(name)
	if err != nil {
		log.Fatal(err)
	}
	defer rd.Close()
	return digest(rd)
}

func printDigest(wr io.Writer, digest []byte) {
	str := b64enc.EncodeToString(digest)
	switch htmlElem {
	case "script":
		fmt.Printf(`<script src="https://example.com/example-framework.js"
        integrity="%s-%s"
        crossorigin="anonymous"></script>`, hashName, str)
	case "link":
		fmt.Printf(`<link rel="stylesheet" href="https://example.com/example-style.css"
      integrity="%s-%s"
      crossorigin="anonymous"></script>`, hashName, str)
	default:
		fmt.Printf("%s-%s", hashName, str)
	}
}

func usage() {
	wr := flag.CommandLine.Output()
	fmt.Fprintf(wr, "Usage of %s:\n", os.Args[0])
	fmt.Fprint(wr, "Digest functions:")
	for fn := range hashes {
		fmt.Fprintf(wr, " %s", fn)
	}
	fmt.Fprintln(wr)
	flag.PrintDefaults()
}

func main() {
	flag.Usage = usage
	flag.StringVar(&hashName, "d", hashName, "Choose digest function")
	flag.StringVar(&htmlElem, "html", htmlElem, "Write HTML element script or link")
	flag.Parse()
	if htmlElem != "" {
		log.Println("Read https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity#cross-origin_resource_sharing_and_subresource_integrity")
	}
	if len(flag.Args()) == 0 {
		printDigest(os.Stdout, digest(os.Stdin))
		fmt.Println()
	} else {
		for _, arg := range flag.Args() {
			printDigest(os.Stdout, digestFile(arg))
			fmt.Println()
		}
	}
}
